package com.musalasoft.drone.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;

//@Entity
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@MappedSuperclass()
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public abstract  class Base implements Serializable {

    @Getter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "native")
    @Column(name = "id", nullable = false, unique = true, updatable = false, insertable = false)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    protected Long id;

    @Getter
    @Basic
    @CreationTimestamp
    @Column()
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    protected java.time.LocalDateTime createdAt;

    @Getter
    @Basic
    @UpdateTimestamp
    @Column()
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    protected java.time.LocalDateTime updatedAt;

    public boolean equals(Object object) {

        if (object == this)
            return true;

        if (!(object instanceof final Base a))
            return false;

        if (id != null && a.getId() != null) {
            return id.equals(a.getId());
        }
        return false;
    }

}
