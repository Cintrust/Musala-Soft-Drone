package com.musalasoft.drone.seeder;

public interface Seeder {
    void seed();
}
