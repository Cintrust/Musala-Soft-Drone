package com.musalasoft.drone.seeder;


import com.musalasoft.drone.entity.User;
import com.musalasoft.drone.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserModelSeeder implements Seeder {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserModelSeeder(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void seed() {

        if (userRepository.count() == 0) {
            User user = new User();
            user.setUsername("trust");
            user.setPassword(passwordEncoder.encode("simple"));
            userRepository.save(user);

        }

    }
}
