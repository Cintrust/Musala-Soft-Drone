package com.musalasoft.drone.config.jwt;

import com.musalasoft.drone.exception.InvalidAuthenticationException;
import com.musalasoft.drone.service.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthTokenFilter extends OncePerRequestFilter {

    /**
     * * {@value }   regex
     * the regular expression to which this string is to be matched
     */
    private String matchUrlPattern = ".*";
    private String excludeUrlPattern = "register|login";
    private final JwtUtils jwtUtils;

    private final AuthService authService;

    private static final Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);

    public AuthTokenFilter(JwtUtils jwtUtils, AuthService authService) {
        this.jwtUtils = jwtUtils;
        this.authService = authService;
    }

    /**
     * @param matchUrlPattern   url regular expression to match
     * @param excludeUrlPattern url regular expression to exclude
     */
    public void setUrlPattern(String matchUrlPattern, String excludeUrlPattern) {

        this.matchUrlPattern = matchUrlPattern;
        this.excludeUrlPattern = excludeUrlPattern;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        boolean authorized = false;
        try {

            String jwt = parseJwt(request);
            if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
                String username = jwtUtils.getUserNameFromJwtToken(jwt);

                UserDetails userDetails = authService.loadUserByUsername(username);
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, jwt, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(authentication);
                authorized = true;
            }
        } catch (Exception e) {
            logger.error("Cannot set user authentication: {}", e.getMessage());
        }

        if (!authorized && matches(request)) {
            throw new InvalidAuthenticationException();
        }

        filterChain.doFilter(request, response);
    }

    boolean matches(HttpServletRequest request) {
        String uri = request.getRequestURI();

        return (!uri.matches(excludeUrlPattern) && uri.matches(matchUrlPattern));
    }

    private String parseJwt(HttpServletRequest request) {


        String headerAuth = request.getHeader("Authorization");

        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(7);
        }

        return null;
    }
}
