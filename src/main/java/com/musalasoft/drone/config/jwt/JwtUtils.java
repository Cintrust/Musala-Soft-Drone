package com.musalasoft.drone.config.jwt;

import com.musalasoft.drone.entity.User;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;


@Component
public class JwtUtils {


    private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

    @Value("${musala.app.jwtSecret}")
    private String jwtSecret;





    @Value("${musala.app.jwtExpirationMs}")
    private Long jwtExpirationMs;



    public String generateJwtToken(Authentication authentication) {

        return generateJwtToken(authentication, false);
    }


    public String generateJwtToken(Authentication authentication, Boolean rememberMe) {

        User userPrincipal = (User) authentication.getPrincipal();

        int factor = 1;
        if (rememberMe != null && rememberMe)
            factor = 7;
        return Jwts.builder()
                .setSubject((userPrincipal.getUsername()))
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + (jwtExpirationMs * factor)))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }


    protected Jws<Claims> parseJwt(String token) {
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
    }

    public String getUserNameFromJwtToken(String token) {
        return parseJwt(token).getBody().getSubject();
    }

    protected static String formatKey(String key) {
        return String.format("token:%s", key);
    }




    public boolean validateJwtToken(String authToken) {
        try {
            parseJwt(authToken);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            logger.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            logger.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty: {}", e.getMessage());
        }

        return false;
    }

    public String parseJwt(HttpServletRequest request) {


        String headerAuth = request.getHeader("Authorization");

        if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
            return headerAuth.substring(7);
        }

        return null;
    }
}
