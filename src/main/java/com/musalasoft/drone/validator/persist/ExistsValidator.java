package com.musalasoft.drone.validator.persist;

import com.musalasoft.drone.validator.annotations.persistent.Exists;
import org.hibernate.validator.internal.engine.constraintvalidation.ConstraintValidatorContextImpl;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class ExistsValidator extends Occurrence implements ConstraintValidator<Exists, Object> {

    protected Exists constraintAnnotation;

    public ExistsValidator(EntityManager entityManager) {
        super(entityManager);
    }

    /**
     * Initializes the validator in preparation for
     * {@link #isValid(Object, ConstraintValidatorContext)} calls.
     * The constraint annotation for a given constraint declaration
     * is passed.
     * <p>
     * This method is guaranteed to be called before any use of this instance for
     * validation.
     * <p>
     * The default implementation is a no-op.
     *
     * @param constraintAnnotation annotation instance for a given constraint declaration
     */
    @Override
    public void initialize(Exists constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    /**
     * Implements the validation logic.
     * The state of {@code value} must not be altered.
     * <p>
     * This method can be accessed concurrently, thread-safety must be ensured
     * by the implementation.
     *
     * @param value   object to validate
     * @param context context in which the constraint is evaluated
     * @return {@code false} if {@code value} does not pass the constraint
     */
    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {

        if (value != null) {
            String attributeName = constraintAnnotation.field();
            Long count = constraintAnnotation.count();
            Long result;
            if (!StringUtils.hasText(attributeName)) {
                result = count(value, (ConstraintValidatorContextImpl) context, constraintAnnotation.entity());
            } else {
                result = count(value, attributeName, constraintAnnotation.entity());
            }
            return Objects.equals(result, count);

        }

        return !constraintAnnotation.required();
    }
}
