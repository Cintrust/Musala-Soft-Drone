package com.musalasoft.drone.validator;

import com.musalasoft.drone.validator.annotations.NotEmptyOrBlank;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotEmptyOrBlankValidator implements
        ConstraintValidator<NotEmptyOrBlank, String> {

    private NotEmptyOrBlank constraintAnnotation;


    @Override
    public void initialize(NotEmptyOrBlank constraintAnnotation) {
        this.constraintAnnotation = constraintAnnotation;
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    /**
     * Implements the validation logic.
     * The state of {@code value} must not be altered.
     * <p>
     * This method can be accessed concurrently, thread-safety must be ensured
     * by the implementation.
     *
     * @param value   object to validate
     * @param context context in which the constraint is evaluated
     * @return {@code false} if {@code value} does not pass the constraint
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {


        return (value != null && !value.trim().isEmpty()) || ((!constraintAnnotation.required() && value == null));
    }
}
