package com.musalasoft.drone.validator.annotations;

import com.musalasoft.drone.validator.NotEmptyOrBlankValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = NotEmptyOrBlankValidator.class)
@Target( {  ElementType.PARAMETER, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface NotEmptyOrBlank {
    String message() default "input must not be empty or blank";
    boolean required() default false;
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
