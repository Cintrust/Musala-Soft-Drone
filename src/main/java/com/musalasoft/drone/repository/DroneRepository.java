package com.musalasoft.drone.repository;

import com.musalasoft.drone.entity.Drone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DroneRepository extends JpaRepository<Drone, Long> {

    Drone getBySerialNumber(String s);

    List<Drone> findAllByState(Drone.State state);
}