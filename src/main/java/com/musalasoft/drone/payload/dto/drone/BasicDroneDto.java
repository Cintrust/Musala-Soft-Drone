package com.musalasoft.drone.payload.dto.drone;

import com.musalasoft.drone.entity.Drone;
import lombok.Getter;

public class BasicDroneDto {

    @Getter
    private final Long id;

    @Getter
    private final String serialNumber;

    @Getter
    private final Integer weightLimit;

    @Getter
    private final Integer batteryCapacity;

    @Getter
    private final Drone.Model model;

    @Getter
    private final Drone.State state;


    public BasicDroneDto(Long id, String serialNumber, Integer weightLimit, Integer batteryCapacity, Drone.Model model, Drone.State state) {
        this.id = id;
        this.serialNumber = serialNumber;
        this.weightLimit = weightLimit;
        this.batteryCapacity = batteryCapacity;
        this.model = model;
        this.state = state;
    }

    public BasicDroneDto(Drone drone){
        id = drone.getId();
        serialNumber= drone.getSerialNumber();
        weightLimit = drone.getWeightLimit();
        batteryCapacity = drone.getBatteryCapacity();
        model = drone.getModel();
        state = drone.getState();
    }
}
