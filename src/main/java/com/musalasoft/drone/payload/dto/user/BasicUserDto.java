package com.musalasoft.drone.payload.dto.user;

import com.musalasoft.drone.entity.User;
import lombok.Getter;

public class BasicUserDto {
    @Getter
    private final Long id;

    @Getter
    private final String username;

    public BasicUserDto(Long id, String username) {
        this.id = id;
        this.username = username;
    }

    public BasicUserDto(User user) {
        id = user.getId();
        username = user.getUsername();
    }
}
