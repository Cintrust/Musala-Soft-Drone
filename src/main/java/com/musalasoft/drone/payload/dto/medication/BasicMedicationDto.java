package com.musalasoft.drone.payload.dto.medication;

import com.musalasoft.drone.entity.Medication;
import lombok.Getter;

public class BasicMedicationDto {

    @Getter
    private final Long id;

    @Getter
    private final String name;

    @Getter
    private final Integer weight;
    ;

    @Getter
    private final String code;

    @Getter
    private final String imageUrl;

    @Getter
    private final Boolean isDelivered;

    public BasicMedicationDto(Long id, String name, Integer weight, String code, String imageUrl, Boolean isDelivered) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.code = code;
        this.imageUrl = imageUrl;
        this.isDelivered = isDelivered;
    }


    public BasicMedicationDto(Medication medication) {

        id = medication.getId();
        name = medication.getName();
        weight = medication.getWeight();
        code = medication.getCode();
        imageUrl = medication.getImageUrl();
        isDelivered = medication.getIsDelivered();

    }
}
