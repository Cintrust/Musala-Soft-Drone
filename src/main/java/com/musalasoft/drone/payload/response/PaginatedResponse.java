package com.musalasoft.drone.payload.response;

import java.util.HashMap;
import java.util.Map;

public class PaginatedResponse extends SuccessResponse {
    private final Map<String, Object> pagination = new HashMap<>();

    public PaginatedResponse() {
        addDataValue("pagination", pagination);
        setTotalPages(0L);
        setCurrentPage(0);
    }



   public void setTotalPages(Long total) {
        pagination.put("totalPages", total);
    }

   public void setCurrentPage(int currentPage) {
        pagination.put("currentPage", currentPage);
    }
}
