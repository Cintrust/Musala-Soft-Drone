package com.musalasoft.drone.payload.response;

public class ErrorResponse extends Response{
    public ErrorResponse() {
        super(Status.Error);
        setMessage("request failed");
    }
}
