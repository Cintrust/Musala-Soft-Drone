package com.musalasoft.drone.payload.response;

public class SuccessResponse extends Response{
    public SuccessResponse() {
        super(Status.Success);
        setMessage("request was successful");
    }
}
