package com.musalasoft.drone.payload.request.auth;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class LoginRequest {

    @NotBlank(message = "password must not be blank")
    @Length(min = 6, max = 30)
    @NotNull(message = "password is required")
    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    @NotBlank
    @Length(min = 3, max = 30)
    private String username;


    @Getter
    @Setter
    private Boolean rememberMe = false;

}
