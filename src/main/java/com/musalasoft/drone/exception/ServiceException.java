package com.musalasoft.drone.exception;

public class ServiceException extends RuntimeException{

    public ServiceException(String msg){
        super(msg);
    }
}
