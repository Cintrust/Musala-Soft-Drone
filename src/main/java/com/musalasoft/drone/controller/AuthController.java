package com.musalasoft.drone.controller;

import com.musalasoft.drone.config.jwt.JwtUtils;
import com.musalasoft.drone.entity.User;
import com.musalasoft.drone.payload.dto.user.BasicUserDto;
import com.musalasoft.drone.payload.request.auth.LoginRequest;
import com.musalasoft.drone.payload.response.Response;
import com.musalasoft.drone.payload.response.SuccessResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping({"api/v1/auth"})
public class AuthController {

    private final AuthenticationManager authenticationManager;


    private final JwtUtils jwtUtils;

    public AuthController(AuthenticationManager authenticationManager, JwtUtils jwtUtils) {
        this.authenticationManager = authenticationManager;

        this.jwtUtils = jwtUtils;
    }


    @PostMapping(path = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> login(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        User userDetails = (User) authentication.getPrincipal();


        String jwt = jwtUtils.generateJwtToken(authentication, loginRequest.getRememberMe());


        SuccessResponse response = new SuccessResponse();
        response.addDataValue("user", new BasicUserDto(userDetails));
        response.addDataValue("jwtToken", jwt);

        return new ResponseEntity<>(response, HttpStatus.OK);

    }


}
