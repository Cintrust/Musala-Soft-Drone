package com.musalasoft.drone.controller;

import com.musalasoft.drone.entity.Drone;
import com.musalasoft.drone.payload.dto.drone.BasicDroneDto;
import com.musalasoft.drone.payload.dto.medication.BasicMedicationDto;
import com.musalasoft.drone.payload.request.drone.CreateDroneRequest;
import com.musalasoft.drone.payload.request.medication.CreateMedicationRequest;
import com.musalasoft.drone.payload.response.Response;
import com.musalasoft.drone.payload.response.SuccessResponse;
import com.musalasoft.drone.service.DroneService;
import com.musalasoft.drone.service.MedicationService;
import com.musalasoft.drone.validator.annotations.persistent.Exists;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping({"api/v1/drones"})
@Validated
public class DroneController {

    private final DroneService droneService;
    private final MedicationService medicationService;

    public DroneController(DroneService droneService, MedicationService medicationService) {
        this.droneService = droneService;
        this.medicationService = medicationService;
    }

    @PostMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> create(@Valid @RequestBody CreateDroneRequest createDroneRequest) {
        Drone drone = droneService.createDrone(createDroneRequest);
        SuccessResponse response = new SuccessResponse();

        response.addDataValue("drone", new BasicDroneDto(drone));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> drones() {

        SuccessResponse response = new SuccessResponse();
        response.addDataValue("drones", droneService.getAvailableDrones());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(path = "/{serialNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> drone(@PathVariable @Valid String serialNumber) {

        SuccessResponse response = new SuccessResponse();
        Drone drone = droneService.getDroneBySerialNumber(serialNumber);
        response.addDataValue("drone", drone != null ? new BasicDroneDto(drone) : null);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(path = "/{serialNumber}/medications", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> addMedications(@PathVariable @Valid @Exists(field = "serialNumber", entity = Drone.class) String serialNumber, @Valid @NotNull @NotEmpty @RequestBody List<CreateMedicationRequest> createMedicationRequestList) {
        medicationService.addMedication(createMedicationRequestList, serialNumber);
        SuccessResponse response = new SuccessResponse();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(path = "/{serialNumber}/medications", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> addMedications(@PathVariable @Valid String serialNumber) {
        List<BasicMedicationDto> medications = medicationService.getMedications(serialNumber);
        SuccessResponse response = new SuccessResponse();

        response.addDataValue("medications", medications);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
