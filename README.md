## MUSALA Drone Service

[[_TOC_]]

### Introduction

This assessment was completed using spring boot framework and h2 as InMemoryDB.
Database is automatically seeded with 4 drones on application start up. 
The application behavior can be fine-tuned by tweaking the 
spring properties files found in `src/main/resources`. 
Some important properties include the following:

| property                       |    sample value    | description                                          |
|--------------------------------|:------------------:|------------------------------------------------------|
| `musala.app.maxDrones`         |         10         | Maximum Number of Drones Allowed  on The System      |
| `musala.app.simulationDelayMs` |        5000        | Delay in Milliseconds Between Each CronJob Execution |
| `logging.level.com.musalasoft` |        INFO        | Log Level                                            |
| `logging.file.name`            | `./logs/drone.log` | Path to Drone Log/Audit file                         |
| `server.port`                  |        8123        | server http port                                     |


### Requirements
- java 17
- set `JAVA_HOME` env to java installation directory


### Build/Run instruction
- open terminal in project root and run `./mvnw spring-boot:run`
- Database is automatically seeded with 4 drones on application start up
- visit `./log/drone.log` to see the audit log

[//]: # (### Run Test)

### Documentation
Http request can be sent to the server on the port specified by `server.port` property.

The Documentation for all sever endpoints can be found here
[Musala Soft Drone Postman Documentation](https://documenter.getpostman.com/view/6428343/UzXKXKFs)

[![Run in Postman](https://run.pstmn.io/button.svg)](https://god.gw.postman.com/run-collection/6428343-749ff2f9-bb12-4f86-be14-026b6c8923a8?action=collection%2Ffork&collection-url=entityId%3D6428343-749ff2f9-bb12-4f86-be14-026b6c8923a8%26entityType%3Dcollection%26workspaceId%3D6c861914-3e16-43e6-a761-58bdfac71101)

